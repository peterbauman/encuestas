package com.arboleda;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class ArboledaApp extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		
		SharedPreferences preferences = getSharedPreferences("appData", MODE_PRIVATE);
		boolean db_instalada = preferences.getBoolean("db_instalada", false);
		if(!db_instalada) {
			copiarbd();
			Editor edit = preferences.edit();
			edit.putBoolean("db_instalada", true);
			edit.commit();
		}
	}
	
	private static final String DATABASE_NAME = "arboles.db";
	//context.getExternalFilesDir(DB_NAME)	
	public static final String DB_PATH = Environment.getDataDirectory().getPath()+"/data/com.arboleda/databases/";
	public static final String DATABASE_NAME_FULLPATH = DB_PATH + DATABASE_NAME;
	
    private void copiarbd() {
        try {
            InputStream in = this.getAssets().open(DATABASE_NAME);
            String ruta = DATABASE_NAME_FULLPATH;
            
            SQLiteDatabase db = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
            db.close();

            OutputStream salida = new FileOutputStream(ruta);
            //openFileOutput("databases/" + DATABASE_NAME, MODE_PRIVATE); 
            byte[] buffer = new byte[1024];
            int tam;
            while ((tam = in.read(buffer)) > 0) {
                salida.write(buffer, 0, tam);
            }
            salida.flush();
            salida.close();
            in.close();
        } catch (Exception e) {
        	e.printStackTrace();
        }

    }
}
