package com.arboleda.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Registro {
	public int getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(int idRegistro) {
		this.idRegistro = idRegistro;
	}
	public String getIdEquipo() {
		return idEquipo;
	}
	public void setIdEquipo(String idEquipo) {
		this.idEquipo = idEquipo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getAlturacalle() {
		return alturacalle;
	}
	public void setAlturacalle(String alturacalle) {
		this.alturacalle = alturacalle;
	}
	public String getIdespecie() {
		return idespecie;
	}
	public void setIdespecie(String idespecie) {
		this.idespecie = idespecie;
	}
	public String getLugardisponible_arbol() {
		return lugardisponible_arbol;
	}
	public void setLugardisponible_arbol(String lugardisponible_arbol) {
		this.lugardisponible_arbol = lugardisponible_arbol;
	}
	public String getTipoubicacion() {
		return tipoubicacion;
	}
	public void setTipoubicacion(String tipoubicacion) {
		this.tipoubicacion = tipoubicacion;
	}
	public String getCircunferenciadeltallo() {
		return circunferenciadeltallo;
	}
	public void setCircunferenciadeltallo(String circunferenciadeltallo) {
		this.circunferenciadeltallo = circunferenciadeltallo;
	}
	public String getAlturaarbol() {
		return alturaarbol;
	}
	public void setAlturaarbol(String alturaarbol) {
		this.alturaarbol = alturaarbol;
	}
	public String getAnchovereda() {
		return anchovereda;
	}
	public void setAnchovereda(String anchovereda) {
		this.anchovereda = anchovereda;
	}
	public String getDlme() {
		return dlme;
	}
	public void setDlme(String dlme) {
		this.dlme = dlme;
	}
	public String getRamasdanadas() {
		return ramasdanadas;
	}
	public void setRamasdanadas(String ramasdanadas) {
		this.ramasdanadas = ramasdanadas;
	}
	public String getTroncodanado() {
		return troncodanado;
	}
	public void setTroncodanado(String troncodanado) {
		this.troncodanado = troncodanado;
	}
	public String getElementosexternos() {
		return elementosexternos;
	}
	public void setElementosexternos(String elementosexternos) {
		this.elementosexternos = elementosexternos;
	}
	public String getInclinacionpel() {
		return inclinacionpel;
	}
	public void setInclinacionpel(String inclinacionpel) {
		this.inclinacionpel = inclinacionpel;
	}
	public String getDanoporraices() {
		return danoporraices;
	}
	public void setDanoporraices(String danoporraices) {
		this.danoporraices = danoporraices;
	}
	public String getProblemasfito() {
		return problemasfito;
	}
	public void setProblemasfito(String problemasfito) {
		this.problemasfito = problemasfito;
	}
	public String getOtros() {
		return otros;
	}
	public void setOtros(String otros) {
		this.otros = otros;
	}
	public String getEstadogeneral() {
		return estadogeneral;
	}
	public void setEstadogeneral(String estadogeneral) {
		this.estadogeneral = estadogeneral;
	}
	public String getRequieretrab() {
		return requieretrab;
	}
	public void setRequieretrab(String requieretrab) {
		this.requieretrab = requieretrab;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getLineamediate() {
		return lineamediate;
	}
	public void setLineamediate(String lineamediate) {
		this.lineamediate = lineamediate;
	}
	public String getZanja() {
		return zanja;
	}
	public void setZanja(String zanja) {
		this.zanja = zanja;
	}
	public String getIdencuestador() {
		return idencuestador;
	}
	public void setIdencuestador(String idencuestador) {
		this.idencuestador = idencuestador;
	}
	public String getIdrtecnico() {
		return idrtecnico;
	}
	public void setIdrtecnico(String idrtecnico) {
		this.idrtecnico = idrtecnico;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public String getBis() {
		return bis;
	}
	public void setBis(String bis) {
		this.bis = bis;
	}
	public String getPar() {
		return par;
	}
	public void setPar(String par) {
		this.par = par;
	}
	public String getNroubicacion() {
		return nroubicacion;
	}
	public void setNroubicacion(String nroubicacion) {
		this.nroubicacion = nroubicacion;
	}
	public String getIdfoto() {
		return idfoto;
	}
	public void setIdfoto(String idfoto) {
		this.idfoto = idfoto;
	}
	public String getRud() {
		return rud;
	}
	public void setRud(String rud) {
		this.rud = rud;
	}
	public String getExactitud() {
		return exactitud;
	}
	public void setExactitud(String exactitud) {
		this.exactitud = exactitud;
	}
	public String getEsquina() {
		return esquina;
	}
	public void setEsquina(String esquina) {
		this.esquina = esquina;
	}
	public String getIntalumpub() {
		return intalumpub;
	}
	public void setIntalumpub(String intalumpub) {
		this.intalumpub = intalumpub;
	}
	public String getIdcalle() {
		return idcalle;
	}
	public void setIdcalle(String idcalle) {
		this.idcalle = idcalle;
	}
	public String getIdesquina() {
		return idesquina;
	}
	public void setIdesquina(String idesquina) {
		this.idesquina = idesquina;
	}
	public String getLetra() {
		return letra;
	}
	public void setLetra(String letra) {
		this.letra = letra;
	}


	public static final String IDEQUIPO = "idEquipo";
	public static final String IDREGISTRO = "_idregistro";
	public static final String FECHA = "fecha";
	public static final String CALLE = "calle";
	public static final String ALTURACALLE = "alturacalle";
	public static final String BIS = "bis";
	public static final String PAR = "par";
	public static final String NROUBICACION = "nroubicacion";
	public static final String IDESPECIE = "idespecie";
	public static final String LUGARDISPONIBLE_ARBOL = "lugardisponible_arbol";
	public static final String TIPOUBICACION = "tipoubicacion";
	public static final String CIRCUNFERENCIADELTALLO = "circunferenciadeltallo";
	public static final String ALTURAARBOL = "alturaarbol";
	public static final String ANCHOVEREDA = "anchovereda";
	public static final String RUD = "rud";
	public static final String DLME = "dlme";
	public static final String RAMASDANADAS = "ramasdanadas";
	public static final String TRONCODANADO = "troncodanado";
	public static final String ELEMENTOSEXTERNOS = "elementosexternos";
	public static final String INCLINACIONPEL = "inclinacionpel";
	public static final String DANOPORRAICES = "danosporraices";
	public static final String PROBLEMASFITO = "problemasfito";
	public static final String OTROS = "otros";
	public static final String ESTADOGENERAL = "estadogeneral";
	public static final String REQUIERETRAB = "requieretrab";
	public static final String OBSERVACIONES = "observaciones";
	public static final String LINEAMEDIATE = "lineamediate";
	public static final String ZANJA = "zanja";
	public static final String IDENCUESTADOR = "idencuestador";
	public static final String IDRTECNICO = "idrtecnico";
	public static final String IDFOTO = "idfoto";
	public static final String LATITUD = "latitud";
	public static final String LONGITUD = "longitud";
	public static final String DATETIME = "datetime";
	public static final String EXACTITUD = "exactitud";	
	public static final String ESQUINA = "esquina";	
	public static final String INTALUMPUB = "intalumpub";
	public static final String IDCALLE = "idcalle";
	public static final String IDESQUINA = "idesquina";
	public static final String LETRA = "letra";
	
	@DatabaseField(generatedId = true, columnName = IDREGISTRO)
	private int idRegistro;
	@DatabaseField(columnName = IDEQUIPO)
	private String idEquipo;
	@DatabaseField(columnName = FECHA)
	private String fecha;
	@DatabaseField(columnName = CALLE)
	private String calle;
	@DatabaseField(columnName = ALTURACALLE)
	private String alturacalle;
	@DatabaseField(columnName = BIS)
	private String bis;
	@DatabaseField(columnName = PAR)
	private String par;
	@DatabaseField(columnName = NROUBICACION)
	private String nroubicacion;
	@DatabaseField(columnName = IDESPECIE)
	private String idespecie;
	@DatabaseField(columnName = LUGARDISPONIBLE_ARBOL)
	private String lugardisponible_arbol;
	@DatabaseField(columnName = TIPOUBICACION)
	private String tipoubicacion;
	@DatabaseField(columnName = CIRCUNFERENCIADELTALLO)
	private String circunferenciadeltallo;
	@DatabaseField(columnName = ALTURAARBOL)
	private String alturaarbol;
	@DatabaseField(columnName = ANCHOVEREDA)
	private String anchovereda;
	@DatabaseField(columnName = RUD)
	private String rud;
	@DatabaseField(columnName = DLME)
	private String dlme;
	@DatabaseField(columnName = RAMASDANADAS)
	private String ramasdanadas;
	@DatabaseField(columnName = TRONCODANADO)
	private String troncodanado;
	@DatabaseField(columnName = ELEMENTOSEXTERNOS)
	private String elementosexternos;
	@DatabaseField(columnName = INCLINACIONPEL)
	private String inclinacionpel;
	@DatabaseField(columnName = DANOPORRAICES)
	private String danoporraices;
	@DatabaseField(columnName = PROBLEMASFITO)
	private String problemasfito;
	@DatabaseField(columnName = OTROS)
	private String otros;
	@DatabaseField(columnName = ESTADOGENERAL)
	private String estadogeneral;
	@DatabaseField(columnName = REQUIERETRAB)
	private String requieretrab;
	@DatabaseField(columnName = OBSERVACIONES)
	private String observaciones;
	@DatabaseField(columnName = LINEAMEDIATE)
	private String lineamediate;
	@DatabaseField(columnName = ZANJA)
	private String zanja;
	@DatabaseField(columnName = IDENCUESTADOR)
	private String idencuestador;
	@DatabaseField(columnName = IDRTECNICO)
	private String idrtecnico;
	@DatabaseField(columnName = IDFOTO)
	private String idfoto;
	@DatabaseField(columnName = LATITUD)
	private String latitud;
	@DatabaseField(columnName = LONGITUD)
	private String longitud;
	@DatabaseField(columnName = DATETIME)
	private String datetime;
	@DatabaseField(columnName = EXACTITUD)
	private String exactitud;
	@DatabaseField(columnName = ESQUINA)
	private String esquina;
	@DatabaseField(columnName = INTALUMPUB)
	private String intalumpub;
	@DatabaseField(columnName = IDCALLE)
	private String idcalle;
	@DatabaseField(columnName = IDESQUINA)
	private String idesquina;
	@DatabaseField(columnName = LETRA)
	private String letra;
	
}
