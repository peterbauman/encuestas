package com.arboleda.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Calle {

	public static final String ID = "_id";
	public static final String DESCRIPCION = "descripcion";
	public static final String DESDE = "desde";
	public static final String HASTA = "hasta";

	@DatabaseField(generatedId = true, columnName = ID)
	private int id;
	@DatabaseField(columnName = DESCRIPCION)
	private String descripcion;
	@DatabaseField(columnName = DESDE)
	private String desde;
	@DatabaseField(columnName = HASTA)
	private String hasta;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDesde() {
		return desde;
	}

	public void setDesde(String desde) {
		this.desde = desde;
	}

	public String getHasta() {
		return hasta;
	}

	public void setHasta(String hasta) {
		this.hasta = hasta;
	}

}