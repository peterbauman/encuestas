package com.arboleda.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Censista {

	public static final String ID = "_id";
	public static final String NOMBRE = "nombre";
	public static final String APELLIDO = "apellido";
	public static final String RTECNICO = "rtecnico";
	public static final String CONTRASENIA = "pass";

	@DatabaseField(generatedId = true, columnName = ID)
	private int id;
	@DatabaseField(columnName = NOMBRE)
	private String nombre;
	@DatabaseField(columnName = APELLIDO)
	private String apellido;
	@DatabaseField(foreign = true, columnName = RTECNICO)
	private RTecnico rtecnico;
	@DatabaseField(columnName = CONTRASENIA)
	private String pass;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	
	public RTecnico getRTecnico() {
		return rtecnico;
	}

	public void setRTecnico(RTecnico rtecnico) {
		this.rtecnico = rtecnico;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	

}
