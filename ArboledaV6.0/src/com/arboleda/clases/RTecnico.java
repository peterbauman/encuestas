package com.arboleda.clases;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class RTecnico {
	public static final String ID = "_id";
	public static final String NOMBRE = "nombre";
	public static final String APELLIDO = "apellido";
	public static final String COMENTARIO = "comentario";

	@DatabaseField(generatedId = true, columnName = ID)
	private int id;
	@DatabaseField(columnName = NOMBRE)
	private String nombre;
	@DatabaseField(columnName = APELLIDO)
	private String apellido;
	@DatabaseField(columnName = COMENTARIO)
	private String comentario;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

}