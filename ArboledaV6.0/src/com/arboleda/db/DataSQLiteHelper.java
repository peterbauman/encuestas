package com.arboleda.db;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.arboleda.clases.Calle;
import com.arboleda.clases.Censista;
import com.arboleda.clases.Especie;
import com.arboleda.clases.RTecnico;
import com.arboleda.clases.Registro;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
 
public class DataSQLiteHelper extends OrmLiteSqliteOpenHelper {
//	Nombre de la DB con fecha
//	private static final SimpleDateFormat dateFormat3 = new SimpleDateFormat("yyyyMMdd");
//	private static final String date3 = dateFormat3.format(new Date());	
//	private String idEqui = Secure.getString(getContentResolver(), Secure.ANDROID_ID); // esto no funciona
//	private static final String DATABASE_NAME = idEqui + "_" + date3 + ".db";
	
	
	
	private static final String DATABASE_NAME = "arboles.db";
	//context.getExternalFilesDir(DB_NAME)	
	public static final String DB_PATH = Environment.getDataDirectory().getPath()+"/data/com.arboleda/databases/";
	public static final String DATABASE_NAME_FULLPATH = DB_PATH + DATABASE_NAME;
    
    private static final int DATABASE_VERSION = 1;
 
    private Dao<Calle, Integer> calleDao;
    private Dao<Censista, Integer> censistaDao;
    private Dao<Especie, Integer> especieDao;
    private Dao<Registro, Integer> registroDao;
    private Dao<RTecnico, Integer> rtecnicoDao;
 
    public DataSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        /*
		TableUtils.createTable(connectionSource, Calle.class);
		TableUtils.createTable(connectionSource, Censista.class);
		TableUtils.createTable(connectionSource, Especie.class);
		TableUtils.createTable(connectionSource, Registro.class);
		TableUtils.createTable(connectionSource, RTecnico.class);
		*/
		// copiarbd();
    }
 
    
    private boolean compruebabd() {
        boolean checkdb = false;

        String path = DB_PATH + DATABASE_NAME;
        File ficherodb = new File(path);
        checkdb = ficherodb.exists();
        return checkdb;
    }

    public void cread() {
        boolean existe = compruebabd();
        if (existe) {

        } else {
            this.getReadableDatabase();
            copiarbd();
        }

    }

    Context context;
    private void copiarbd() {
        try {
            InputStream in = context.getAssets().open(DATABASE_NAME);
            String ruta = DATABASE_NAME_FULLPATH;

            OutputStream salida = new FileOutputStream(ruta);
            byte[] buffer = new byte[1024];
            int tam;
            while ((tam = in.read(buffer)) > 0) {
                salida.write(buffer, 0, tam);
            }
            salida.flush();
            salida.close();
            in.close();
        } catch (Exception e) {

        }

    }
    
    
    
    
    
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        onCreate(db, connectionSource);
    }
 
    public Dao<Calle, Integer> getCalleDao() throws SQLException {
        if (calleDao == null) {
        	calleDao = getDao(Calle.class);
        }
        return calleDao;
    }
  
    public Dao<Censista, Integer> getCensistaDao() throws SQLException {
        if (censistaDao == null) {
        	censistaDao = getDao(Censista.class);
        }
        return censistaDao;
    }
    
    public Dao<Especie, Integer> getEspecieDao() throws SQLException {
        if (especieDao == null) {
        	especieDao = getDao(Especie.class);
        }
        return especieDao;
    }

    public Dao<Registro, Integer> getRegistroDao() throws SQLException {
        if (registroDao == null) {
        	registroDao = getDao(Registro.class);
        }
        return registroDao;
    }
        
    public Dao<RTecnico, Integer> getRTecnicoDao() throws SQLException {
        if (rtecnicoDao == null) {
        	rtecnicoDao = getDao(RTecnico.class);
        }
        return rtecnicoDao;
    }
    
    @Override
    public void close() {
        super.close();
        calleDao = null;
        censistaDao = null;
        especieDao = null;
        registroDao = null;
        rtecnicoDao = null;
    }
 
}
