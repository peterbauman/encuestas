package com.arboleda.db;

import java.util.List;

import android.content.Context;

import com.arboleda.clases.Calle;
import com.arboleda.clases.Censista;
import com.arboleda.clases.Especie;
import com.arboleda.clases.RTecnico;
import com.arboleda.clases.Registro;

public class DatabaseManager {
	static private DatabaseManager instance;

	static public void init(Context ctx) {
		if (null == instance) {
			instance = new DatabaseManager(ctx);
		}
	}

	static public DatabaseManager getInstance() {
		return instance;
	}

	private DataSQLiteHelper helper;

	private DatabaseManager(Context ctx) {
		helper = new DataSQLiteHelper(ctx);
	}

	private DataSQLiteHelper getHelper() {
		return helper;
	}

	public List<Registro> getAllRegistros() {
		List<Registro> registroList = null;
		try {
			registroList = getHelper().getRegistroDao().queryForAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return registroList;
	}
	
	
	public List<Censista> getAllCensistas() {
		List<Censista> censistaList = null;
		try {
			censistaList = getHelper().getCensistaDao().queryForAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return censistaList;
	}
	
	public void addRegistro(Registro c) {
		try {
			getHelper().getRegistroDao().create(c);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteRegistroByID(int iD) {
		try {
			getHelper().getRegistroDao().deleteById(iD);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Registro getRegistroByName(String idReg) {
		try {
			List<Registro> list = null;
			list = getHelper().getRegistroDao().queryForEq("idRegistro", idReg);
			return list.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Censista getCensistaById(int id) {
		try {
			List<Censista> list = null;
			list = getHelper().getCensistaDao().queryForEq("_id", id);
			return list.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Calle> getAllCalles() {
		List<Calle> calleList = null;
		try {
			calleList = getHelper().getCalleDao().queryForAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return calleList;
	}
	
	public Calle getCalleByDescripcion(String desc) {
		try {
			List<Calle> list = null;
			list = getHelper().getCalleDao().queryForEq("descripcion", desc);
			return list.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}	
	
	public List<RTecnico> getAllRTecnico() {
		List<RTecnico> rTecnicoList = null;
		try {
			rTecnicoList = getHelper().getRTecnicoDao().queryForAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rTecnicoList;
	}
	
	public RTecnico getRTecnicoById(int id) {
		try {
			List<RTecnico> list = null;
			list = getHelper().getRTecnicoDao().queryForEq("_id", id);
			return list.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}	
	
	public List<Especie> getAllEspecie() {
		List<Especie> especieList = null;
		try {
			especieList = getHelper().getEspecieDao().queryForAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return especieList;
	}
	
	public Especie getEspecieById(int id) {
		try {
			List<Especie> list = null;
			list = getHelper().getEspecieDao().queryForEq("_id", id);
			return list.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}	


}
