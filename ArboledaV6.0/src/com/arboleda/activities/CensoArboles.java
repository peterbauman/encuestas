package com.arboleda.activities;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.arboleda.R;
import com.arboleda.clases.Especie;
import com.arboleda.clases.Registro;
import com.arboleda.db.DataSQLiteHelper;
import com.arboleda.db.DatabaseManager;

public class CensoArboles extends Activity {

	private DatabaseManager manager;
	List<Registro> registroList;
	List<Especie> especieList;

	static int contador = 0;
	static int nro = 0;

	private String nameFoto;
	private String idFoto = "";

	private EditText latitud;
	private EditText longitud;
	private EditText txtCalle;
	private EditText txtAltura;
	private CheckBox chkBis;
	private CheckBox chkPar;
	private CheckBox chkRamasDaniadas;
	private CheckBox chkTroncoDaniado;
	private CheckBox chkElementosExtranios;
	private CheckBox chkInclinacionPeligrosa;
	private CheckBox chkRequiereTrabajos;
	private CheckBox chkSanja;
	private CheckBox chkLineaMedTension;
	private CheckBox chkIntAlumPub;
	private RadioButton rbArbol;
	private RadioButton rbLugarDisponible;
	private RadioButton rbNoDisponible;
	private RadioGroup rbgLugarDisponible;
	private RadioButton radioBtnSelect;
	private EditText txtAnchoVereda;
	private EditText txtEncuestador;
	// private EditText txtRespTecnico;
	private EditText txtNroUbicacion;
	private EditText txtDlme;
	private EditText txtRud;
	private EditText txtCircTallo;
	private EditText txtOtros;
	private EditText txtObservaciones;
	private EditText txtExactitud;
	private Spinner spnTipoUbicacion;
	private Spinner spnEspecie;
	private Spinner spnAltura;
	private Spinner spnDanioRaices;
	private Spinner spnProbFitos;
	private Spinner spnEstadoGral;
	private Spinner spnLetraAltura;

	CensoArboles CameraActivity = null;

	private Button btnGuardar;
	private ImageButton btnFoto;
	private ImageButton btnMapa;
	private ImageButton btnAtras;
	private ImageButton btnAnterior;
	private ImageButton btnSiguiente;
	private ImageButton btnEliminar;
	private ImageButton btnAgregar;
	private ImageButton btnModificar;

	private DataSQLiteHelper mDBHelper;


	private String respTecnico;
	private String fecha;
	private String idCalle;
	private String idEqui;
	private String idEsquina;
	private String esquina;
	// private String altura;

	final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;

	
	private LocationManager locManager;
	private LocationListener locListener;
	private Location mobileLocation;
	
	
	Uri imageUri = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Set portrait orientation
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setContentView(R.layout.censo_arboles);

		manager = DatabaseManager.getInstance();

		CameraActivity = this;
		btnFoto = (ImageButton) findViewById(R.id.imgBtnFoto);
		btnMapa = (ImageButton) findViewById(R.id.imgBtnMapa);
		btnAtras = (ImageButton) findViewById(R.id.imgBtnAtras);
		btnGuardar = (Button) findViewById(R.id.btnGuardar);

		latitud = (EditText) findViewById(R.id.txtLatitud);
		longitud = (EditText) findViewById(R.id.txtLongitud);

		txtCalle = (EditText) findViewById(R.id.txtCalle);
		txtAltura = (EditText) findViewById(R.id.txtAltura);
		chkBis = (CheckBox) findViewById(R.id.chkBis);
		chkPar = (CheckBox) findViewById(R.id.chkPar);
		txtAnchoVereda = (EditText) findViewById(R.id.txtAnchoVereda);
		txtEncuestador = (EditText) findViewById(R.id.txtEncuestador);
		txtNroUbicacion = (EditText) findViewById(R.id.txtNroUbicacion);
		txtDlme = (EditText) findViewById(R.id.txtDLME);
		txtRud = (EditText) findViewById(R.id.txtRUD);
		txtCircTallo = (EditText) findViewById(R.id.txtCircTallo);
		txtOtros = (EditText) findViewById(R.id.txtOtros);
		txtObservaciones = (EditText) findViewById(R.id.txtObservaciones);
		txtExactitud = (EditText) findViewById(R.id.txtExactitud);
		chkRamasDaniadas = (CheckBox) findViewById(R.id.chkRamasDania);
		chkTroncoDaniado = (CheckBox) findViewById(R.id.chkTroncoDania);
		chkElementosExtranios = (CheckBox) findViewById(R.id.chkElemExtranio);
		chkInclinacionPeligrosa = (CheckBox) findViewById(R.id.chkInclinacionPeli);
		chkRequiereTrabajos = (CheckBox) findViewById(R.id.chkRequiereTrab);
		chkSanja = (CheckBox) findViewById(R.id.chkSanja);
		chkLineaMedTension = (CheckBox) findViewById(R.id.chkLinMedTension);
		chkIntAlumPub = (CheckBox) findViewById(R.id.chkAlumPub);
		spnTipoUbicacion = (Spinner) findViewById(R.id.spnTipoUbicacion);
		spnEspecie = (Spinner) findViewById(R.id.spnEspecie);
		spnAltura = (Spinner) findViewById(R.id.spnAltura);
		spnDanioRaices = (Spinner) findViewById(R.id.spnDanioRaices);
		spnProbFitos = (Spinner) findViewById(R.id.spnProbFitosani);
		spnEstadoGral = (Spinner) findViewById(R.id.spnEstadoGral);
		rbArbol = (RadioButton) findViewById(R.id.rbArbol);
		rbLugarDisponible = (RadioButton) findViewById(R.id.rbLugarDisponible);
		rbNoDisponible = (RadioButton) findViewById(R.id.rbNoDisponible);
		rbgLugarDisponible = (RadioGroup) findViewById(R.id.rgArbolDisp);
		spnLetraAltura = (Spinner) findViewById(R.id.spnLetra);

		Bundle extras = getIntent().getExtras();
		fecha = extras.getString("fecha");
		String censista = extras.getString("cencista");
		respTecnico = extras.getString("respTecnico");
		idCalle = extras.getString("idCalle");
		String calle = extras.getString("calle");
		String altura = extras.getString("altura");
		int posLetra = extras.getInt("posLetra");
		Boolean bis = extras.getBoolean("bis");
		Boolean par = extras.getBoolean("par");
		idEsquina = extras.getString("idEsquina");
		esquina = extras.getString("esquina");
		String anchoVereda = extras.getString("anchoVereda");

		txtCalle.setText(calle);
		txtAltura.setText(altura);
		spnLetraAltura.setSelection(posLetra);
		chkBis.setChecked(bis);
		chkPar.setChecked(par);
		txtAnchoVereda.setText(anchoVereda);
		txtEncuestador.setText(censista);
		// txtRespTecnico.setText(respTecnico);

		// Id Equipo
		idEqui = Secure.getString(getContentResolver(), Secure.ANDROID_ID);

		especieList = manager.getAllEspecie();
		List<String> listaEspecieName = new ArrayList<String>();
		for (Especie elemento : especieList) {
			listaEspecieName.add(elemento.getNombre());
		}
		Collections.sort(listaEspecieName);
		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, listaEspecieName);
		// Drop down layout style - list view with radio button
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// attaching data adapter to spinner
		spnEspecie.setAdapter(dataAdapter);

		btnGuardar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!txtNroUbicacion.getText().toString().equals("")) {
					int nroUbicacion = Integer.parseInt(txtNroUbicacion
							.getText().toString());

					if ((nroUbicacion >= Integer.parseInt(txtAltura.getText()
							.toString()))
							&& (nroUbicacion < Integer.parseInt(txtAltura
									.getText().toString()) + 100)) {
						if (chkPar.isChecked() && !esPar(nroUbicacion)) {
							Toast.makeText(getApplicationContext(),
									"Ingrese una ubicaci�n par.",
									Toast.LENGTH_LONG).show();
						}
						if (!chkPar.isChecked() && esPar(nroUbicacion)) {
							Toast.makeText(getApplicationContext(),
									"Ingrese una ubicaci�n impar.",
									Toast.LENGTH_LONG).show();
						}

						if ((chkPar.isChecked() && esPar(nroUbicacion))
								|| (!chkPar.isChecked() && !esPar(nroUbicacion))) {
							registrar();
						}
					} else {
						Toast.makeText(getApplicationContext(),
								"Verifique el n�mero de ubicaci�n.",
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Debe ingresar un n�mero de ubicaci�n.",
							Toast.LENGTH_LONG).show();
				}

			}
		});

		btnMapa.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				buttonGetLocationClick();
		

			}
		});

		btnAtras.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});

		rbgLugarDisponible
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					public void onCheckedChanged(RadioGroup arg0, int id) {
						if (id == R.id.rbArbol) {
							txtAnchoVereda.setEnabled(true);
							spnTipoUbicacion.setEnabled(true);
							txtRud.setEnabled(true);
							txtDlme.setEnabled(true);
							spnEspecie.setEnabled(true);
							txtCircTallo.setEnabled(true);
							spnAltura.setEnabled(true);
							chkElementosExtranios.setEnabled(true);
							chkInclinacionPeligrosa.setEnabled(true);
							chkLineaMedTension.setEnabled(true);
							chkRamasDaniadas.setEnabled(true);
							chkRequiereTrabajos.setEnabled(true);
							chkSanja.setEnabled(true);
							chkTroncoDaniado.setEnabled(true);
							chkIntAlumPub.setEnabled(true);
							spnDanioRaices.setEnabled(true);
							spnProbFitos.setEnabled(true);
							// txtOtros.setEnabled(true);
							spnEstadoGral.setEnabled(true);
							btnFoto.setEnabled(true);
						}
						if (id == R.id.rbLugarDisponible) {
							// borra todos campos que no corresponden si no hay
							// arbol
							spnEspecie.setSelection(0);
							txtCircTallo.setText("");
							spnAltura.setSelection(0);
							chkRamasDaniadas.setChecked(false);
							chkTroncoDaniado.setChecked(false);
							chkElementosExtranios.setChecked(false);
							chkInclinacionPeligrosa.setChecked(false);
							spnDanioRaices.setSelection(0);
							spnProbFitos.setSelection(0);
							txtOtros.setText("");
							spnEstadoGral.setSelection(0);
							chkRequiereTrabajos.setChecked(false);
							chkLineaMedTension.setChecked(false);
							chkIntAlumPub.setChecked(false);
							chkSanja.setChecked(false);

							// deshabilita todo menos GPS y Observaciones
							// txtNroUbicacion.setEnabled(false);
							txtAnchoVereda.setEnabled(false);
							spnTipoUbicacion.setEnabled(false);
							txtRud.setEnabled(false);
							txtDlme.setEnabled(false);
							spnEspecie.setEnabled(false);
							txtCircTallo.setEnabled(false);
							spnAltura.setEnabled(false);
							chkElementosExtranios.setEnabled(false);
							chkInclinacionPeligrosa.setEnabled(false);
							chkLineaMedTension.setEnabled(false);
							chkRamasDaniadas.setEnabled(false);
							chkRequiereTrabajos.setEnabled(false);
							chkSanja.setEnabled(false);
							chkTroncoDaniado.setEnabled(false);
							chkIntAlumPub.setEnabled(false);
							spnDanioRaices.setEnabled(false);
							spnProbFitos.setEnabled(false);
							txtOtros.setEnabled(false);
							spnEstadoGral.setEnabled(false);
							btnFoto.setEnabled(false);
						}

					}
				});

		spnProbFitos.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				String item = parent.getItemAtPosition(position).toString();

				if (item.equals("OTRO")) {
					txtOtros.setEnabled(true);
				}
			}

			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

	

		btnFoto.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Creamos el Intent para llamar a la Camara
				Intent cameraIntent = new Intent(
						android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				// Creamos una carpeta en la memoria del terminal
				File imagesFolder = new File(Environment
						.getExternalStorageDirectory(), "ArboledaFotos");
				imagesFolder.mkdirs();

				nameFoto = getNameFoto();

				// a�adimos el nombre de la imagen
				File image = new File(imagesFolder, nameFoto);

				// escalamos la imagen VER
				// File re_image = resizeImage(CameraActivity,0,0,0);

				Uri uriSavedImage = Uri.fromFile(image);

				// Le decimos al Intent que queremos grabar la imagen
				cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
				// Lanzamos la aplicacion de la camara con retorno (forResult)
				startActivityForResult(cameraIntent, 1);
			}
		});

	}

	// jes
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Comprovamos que la foto se ha realizado
		if (requestCode == 1 && resultCode == RESULT_OK) {
			// Creamos un bitmap con la imagen recientemente almacenada en la
			// memoria
			Bitmap bMap = BitmapFactory.decodeFile(Environment
					.getExternalStorageDirectory()
					+ "/ArboledaFotos/"
					+ nameFoto);
			// Escalamos la imagen
			Bitmap newBitmap = Bitmap.createScaledBitmap(bMap, 107, 98, true);
			// A�adimos el bitmap al imageView para mostrarlo por pantalla
			btnFoto.setImageBitmap(newBitmap);
			idFoto = nameFoto;
		}
	}

	public String getNameFoto() {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddkkmmss");
		String date = dateFormat.format(new Date());
		String photoFile = "img_" + idEqui + "_" + date + ".JPG";
		return photoFile;
	}

	
	static boolean esPar(int numero) {
		if (numero % 2 == 0)
			return true;
		else
			return false;
	}

	public void borrarValoresEditText() {
		txtNroUbicacion.setText("");
		// txtAnchoVereda.setText("");
		txtRud.setText("");
		txtDlme.setText("");
		spnTipoUbicacion.setSelection(0);
		spnEspecie.setSelection(0);
		txtCircTallo.setText("");
		spnAltura.setSelection(0);
		chkRamasDaniadas.setChecked(false);
		chkTroncoDaniado.setChecked(false);
		chkElementosExtranios.setChecked(false);
		chkInclinacionPeligrosa.setChecked(false);
		spnDanioRaices.setSelection(0);
		spnProbFitos.setSelection(0);
		txtOtros.setText("");
		spnEstadoGral.setSelection(0);
		chkRequiereTrabajos.setChecked(false);
		chkLineaMedTension.setChecked(false);
		chkIntAlumPub.setChecked(false);
		chkSanja.setChecked(false);
		latitud.setText("");
		longitud.setText("");
		txtExactitud.setText("");
		txtObservaciones.setText("");
		rbArbol.setChecked(true);
		rbLugarDisponible.setChecked(false);
		rbNoDisponible.setChecked(false);

		btnFoto.setImageResource(this.getResources().getIdentifier(
				"drawable/" + "foto", null, this.getPackageName()));

	}

	public void registrar() {

		AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
		dialogo1.setCancelable(false);
		dialogo1.setTitle("Guardar");
		dialogo1.setMessage("�Guardar los datos en la BD?");
		dialogo1.setPositiveButton("Aceptar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialogo1, int id) {
						Registro reg = new Registro();
						reg.setIdEquipo(String.valueOf(idEqui));
						reg.setFecha(String.valueOf(fecha));
						reg.setCalle(String.valueOf(txtCalle.getText()
								.toString()));

//						String miAltura = txtAltura.getText().toString() + spnLetraAltura.getSelectedItem().toString();
						reg.setAlturacalle(String.valueOf(txtAltura.getText().toString()));
						reg.setBis(String.valueOf(chkBis.isChecked()));
						reg.setPar(String.valueOf(chkPar.isChecked()));

//						String miUbicacion = txtNroUbicacion.getText().toString() + spnLetraAltura.getSelectedItem().toString();
						reg.setNroubicacion(String.valueOf(txtNroUbicacion.getText().toString()));
						reg.setLetra(String.valueOf(spnLetraAltura.getSelectedItem().toString()));
						
						reg.setIdespecie(String.valueOf(spnEspecie
								.getSelectedItem().toString()));

						// RadioGroup
						radioBtnSelect = (RadioButton) findViewById(rbgLugarDisponible
								.getCheckedRadioButtonId());
						reg.setLugardisponible_arbol(String
								.valueOf(radioBtnSelect.getText()));

						reg.setTipoubicacion(String.valueOf(spnTipoUbicacion
								.getSelectedItem().toString()));
						reg.setCircunferenciadeltallo(String
								.valueOf(txtCircTallo.getText().toString()));
						reg.setAlturaarbol(String.valueOf(spnAltura
								.getSelectedItem().toString()));
						reg.setAnchovereda(String.valueOf(txtAnchoVereda
								.getText().toString()));
						reg.setRud(String.valueOf(txtRud.getText().toString()));
						reg.setDlme(String
								.valueOf(txtDlme.getText().toString()));
						reg.setRamasdanadas(String.valueOf(chkRamasDaniadas
								.isChecked()));
						reg.setTroncodanado(String.valueOf(chkTroncoDaniado
								.isChecked()));
						reg.setElementosexternos(String
								.valueOf(chkElementosExtranios.isChecked()));
						reg.setInclinacionpel(String
								.valueOf(chkInclinacionPeligrosa.isChecked()));
						reg.setDanoporraices(String.valueOf(spnDanioRaices
								.getSelectedItem().toString()));
						reg.setProblemasfito(String.valueOf(spnProbFitos
								.getSelectedItem().toString()));
						reg.setOtros(String.valueOf(txtOtros.getText()
								.toString()));
						reg.setEstadogeneral(String.valueOf(spnEstadoGral
								.getSelectedItem().toString()));
						reg.setRequieretrab(String.valueOf(chkRequiereTrabajos
								.isChecked()));
						reg.setObservaciones(String.valueOf(txtObservaciones
								.getText().toString()));
						reg.setLineamediate(String.valueOf(chkLineaMedTension
								.isChecked()));
						reg.setZanja(String.valueOf(chkSanja.isChecked()));
						reg.setIdencuestador(String.valueOf(txtEncuestador
								.getText().toString()));
						// reg.setIdrtecnico(String.valueOf(txtRespTecnico.getText().toString()));
						reg.setIdrtecnico(String.valueOf(respTecnico));
						reg.setIdfoto(String.valueOf(idFoto));
						reg.setLatitud(String.valueOf(latitud.getText()
								.toString()));
						reg.setLongitud(String.valueOf(longitud.getText()
								.toString()));

						// DateTime
						SimpleDateFormat dateFormat2 = new SimpleDateFormat(
								"yyyyMMddkkmmss");
						String date2 = dateFormat2.format(new Date());
						reg.setDatetime(String.valueOf(date2)); // VER

						reg.setExactitud(String.valueOf(txtExactitud.getText()
								.toString()));
						reg.setEsquina(String.valueOf(esquina));
						reg.setIntalumpub(String.valueOf(chkIntAlumPub
								.isChecked()));
						reg.setIdcalle(String.valueOf(idCalle));
						reg.setIdesquina(String.valueOf(idEsquina));

						// borra la pantalla
						borrarValoresEditText();

						manager.addRegistro(reg);
						idFoto = "";
						txtOtros.setEnabled(false);

						Toast.makeText(getApplicationContext(), "Guardando...",
								Toast.LENGTH_LONG).show();
					}
				});
		dialogo1.setNegativeButton("Cancelar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialogo1, int id) {
						// Toast.makeText(getApplicationContext(),
						// "No se guardaron los datos.",
						// Toast.LENGTH_SHORT).show();
						// finish();
					}
				});
		dialogo1.show();
	}
	
	private void getCurrentLocation() {
		locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		locListener = new LocationListener() {
			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				mobileLocation = location;
			}
		};
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
	}
	
	private void buttonGetLocationClick() {
		getCurrentLocation(); // gets the current location and update mobileLocation variable
		
		if (mobileLocation != null) {
			locManager.removeUpdates(locListener); // This needs to stop getting the location data and save the battery power.
			
			//String londitude = "Londitude: " + mobileLocation.getLongitude();
			//String latitude = "Latitude: " + mobileLocation.getLatitude();
			//String altitiude = "Altitiude: " + mobileLocation.getAltitude();
			//String accuracy = "Accuracy: " + mobileLocation.getAccuracy();
			//String time = "Time: " + mobileLocation.getTime();
			latitud.setText(String.valueOf(mobileLocation.getLatitude()));
			longitud.setText(String.valueOf(mobileLocation.getLongitude()));
			txtExactitud.setText(String.valueOf(mobileLocation.getAccuracy()));
		} else {
			
			latitud.setText("Latitud: (sin_datos)");
			longitud.setText("Longitud: (sin_datos)");
			txtExactitud.setText("Exactitud: (sin_datos)");
			//editTextShowLocation.setText("Sorry, location is not determined");
		}
	
	}
	

}
