package com.arboleda.activities;

import com.arboleda.R;
import com.arboleda.SplashScreenService;
 
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Window;

public class SplashScreen extends Activity {
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        // Set portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
 
        setContentView(R.layout.splash_screen);
        
		LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
		lbm.registerReceiver(br, new IntentFilter(SplashScreenService.SPLASH_SCREEN_FINISH_ACTION));
		
		startService(new Intent(this, SplashScreenService.class));
	}
    
	BroadcastReceiver br = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
            // Start the next activity
            Intent mainIntent = new Intent().setClass(
                    SplashScreen.this, Inicio2.class);
            startActivity(mainIntent);
            
			
			// Close the activity so the user won't able to go back this
			// activity pressing Back button
			finish();
		}
	};
	
    @Override
    protected void onStop() {
    	super.onStop();
    	
		LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
		lbm.unregisterReceiver(br);
    }
}