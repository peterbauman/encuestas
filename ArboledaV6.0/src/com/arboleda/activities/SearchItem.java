package com.arboleda.activities;

import android.os.Parcel;
import android.os.Parcelable;

class SearchItem implements Parcelable {
	Integer id;
	String text;
	
	public SearchItem(int id, String text) {
		this.id = id;
		this.text = text;
	}		
	
	public Integer getId() {
		return id;
	}

	public String getText() {
		return text;
	}
	
	@Override
	public String toString() {
		return text;
	}

	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(text);
	}

	public static final Parcelable.Creator<SearchItem> CREATOR = new Parcelable.Creator<SearchItem>() {
		public SearchItem createFromParcel(Parcel in) {
			return new SearchItem(in);
		}

		public SearchItem[] newArray(int size) {
			return new SearchItem[size];
		}
	};

	private SearchItem(Parcel in) {
		id = in.readInt();
		text = in.readString();
	}
}