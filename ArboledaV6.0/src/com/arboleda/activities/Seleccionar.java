package com.arboleda.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.arboleda.R;

public class Seleccionar extends Activity {
	
	private ListView lv;
	
	ArrayAdapter<SearchItem> adapter;
	
	EditText inputSearch;
	

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
   		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        
        setContentView(R.layout.seleccionar);
       Bundle extras = getIntent().getExtras();
        if(extras == null || !extras.containsKey("lista")) {
        	finish();
        	return;
        }
        
        final ArrayList<SearchItem> items = extras.getParcelableArrayList("lista");
        
        lv = (ListView) findViewById(R.id.list_view);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        
        // Adding items to listview
        adapter = new ArrayAdapter<SearchItem>(this, R.layout.list_item, R.id.product_name, items);
        lv.setAdapter(adapter);
        
        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
				// When user changed the Text
				Seleccionar.this.adapter.getFilter().filter(cs);	
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub							
			}
		});
        
        
        
        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                    long id) {
                Intent intent = new Intent();
                
                SearchItem item = adapter.getItem(position);
                
                //String message = items.get(position);
                //Toast.makeText(getApplicationContext(), item.getText(), Toast.LENGTH_LONG).show();
                
                intent.putExtra("item", item);
                //intent.putExtra("valor", message);
                //intent.putExtra("id", itemIds.get(position));
                setResult(RESULT_OK, intent);
                
                finish();
            }
        });
        
        
    }
}
