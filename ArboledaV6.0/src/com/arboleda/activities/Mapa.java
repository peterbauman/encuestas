package com.arboleda.activities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
//import android.view.WindowId.FocusObserver;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import com.arboleda.R;
import com.arboleda.clases.Calle;
import com.arboleda.db.DatabaseManager;

public class Mapa extends Activity {
	private DatabaseManager manager;
	List<Calle> calleList;
	private Calle unaCalle;
	private Calle unaEsquina;
	private String idCalle;
	private String idEsquina;
	
	private ImageButton btnMas;
	private ImageButton btnAtras;
	private TextView tvCalle;
	private EditText txtAltura;
	private CheckBox chkBis;
	private Spinner spnPar;
	private EditText txtAnchoVereda;
	private Boolean par;
	private TextView tvEsquina;
	private Spinner spnLetra;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Set portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		setContentView(R.layout.mapa);
		
		manager = DatabaseManager.getInstance();
		
		btnMas = (ImageButton) findViewById(R.id.imgBtnMas);
		btnAtras = (ImageButton) findViewById(R.id.imgBtnAtras);
		//spnCalle = (Spinner) findViewById(R.id.spnCalle);
		tvCalle = (TextView) findViewById(R.id.spnCalle);
		
		//spnAltura = (Spinner) findViewById(R.id.spnAltura);
		txtAltura = (EditText) findViewById(R.id.txtAltura);
		chkBis = (CheckBox) findViewById(R.id.chkBis);
		//chkPar = (CheckBox) findViewById(R.id.chkPar);
		spnPar = (Spinner) findViewById(R.id.spnPar);
		txtAnchoVereda = (EditText) findViewById(R.id.txtAnchoVereda);
		spnLetra = (Spinner) findViewById(R.id.spnAltura);
		
		//spnEsquina = (Spinner) findViewById(R.id.spnEsquina);
		tvEsquina = (TextView) findViewById(R.id.spnEsquina);
		
		Bundle extras = getIntent().getExtras();
		//String calle = extras.getString("calle");
		String altura = extras.getString("altura");
		Boolean bis = extras.getBoolean("bis");
		par = extras.getBoolean("par");
		String anchoVereda = extras.getString("anchoVereda");
		
		//spnCalle.setSelection(position);
		//spnAltura.setTag(altura);
		txtAltura.setText(altura);
		chkBis.setChecked(bis);
		//chkPar.setChecked(par);
		txtAnchoVereda.setText(anchoVereda);
		
		tvCalle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				calleList = manager.getAllCalles();
				Collections.sort(calleList, new Comparator<Calle>() {
					@Override
					public int compare(Calle lhs, Calle rhs) {
						String l = lhs.getDescripcion();
						String r = rhs.getDescripcion();
						return l.compareToIgnoreCase(r);
					}
				});
				
				ArrayList<SearchItem> listaItems = new ArrayList<SearchItem>();
				
				for(Calle elemento : calleList) {
					listaItems.add(new SearchItem(elemento.getId(), elemento.getDescripcion()));
				}
				
				Intent i = new Intent(Mapa.this, Seleccionar.class);
				
				i.putParcelableArrayListExtra("lista", listaItems);
				
				startActivityForResult(i, 1);
			}
		});
		
		tvEsquina.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				calleList = manager.getAllCalles();
				Collections.sort(calleList, new Comparator<Calle>() {
					@Override
					public int compare(Calle lhs, Calle rhs) {
						String l = lhs.getDescripcion();
						String r = rhs.getDescripcion();
						return l.compareToIgnoreCase(r);
					}
				});
				
				ArrayList<SearchItem> listaItems = new ArrayList<SearchItem>();
				
				for(Calle elemento : calleList) {
					listaItems.add(new SearchItem(elemento.getId(), elemento.getDescripcion()));
				}
				
				Intent i = new Intent(Mapa.this, Seleccionar.class);
				
				i.putParcelableArrayListExtra("lista", listaItems);
				
				startActivityForResult(i, 2);
			}
		});
		     				
		// Spinner click listener
		spnPar.setOnItemSelectedListener(new OnItemSelectedListener() {
    	    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    	    	String item2 = parent.getItemAtPosition(position).toString();
    	    	if(item2.equals("PAR")){
    	    		par = true;
    	    	}
    	    	if(item2.equals("IMPAR")){
    	    		par = false;
    	    	}
    	    }
    	    public void onNothingSelected(AdapterView<?> arg0) {
    	    }
    	});
								
		btnMas.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
								
					if (unaCalle == null){
						Toast.makeText(getApplicationContext(), "Debe seleccionar la calle.", Toast.LENGTH_LONG).show();
					}
					
					if (txtAltura.getText().toString().equals("")){
						Toast.makeText(getApplicationContext(), "Debe ingresar una altura.", Toast.LENGTH_LONG).show();
					}
					
				if (unaCalle != null && !txtAltura.getText().toString().equals("")){		
				String calle = unaCalle.getDescripcion();	
				String altura = txtAltura.getText().toString();
				int posLetra = spnLetra.getSelectedItemPosition();
				Boolean bis = chkBis.isChecked();
				String anchoVereda = txtAnchoVereda.getText().toString();
				String esquina = tvEsquina.getText().toString();
				
				Bundle extras = getIntent().getExtras();
				String fecha = extras.getString("fecha");
				String cencista = extras.getString("cencista");
				String respTecnico = extras.getString("respTecnico");
				
				Intent intent = new Intent(Mapa.this, CensoArboles.class);
				intent.putExtra("fecha", fecha);
				intent.putExtra("cencista", cencista);
				intent.putExtra("respTecnico", respTecnico);
				intent.putExtra("idCalle", idCalle);
				intent.putExtra("calle", calle);
				intent.putExtra("altura", altura);
				intent.putExtra("posLetra", posLetra);
				intent.putExtra("bis", bis);
				intent.putExtra("par", par);
				intent.putExtra("idEsquina", idEsquina);
				intent.putExtra("esquina", esquina);
				intent.putExtra("anchoVereda", anchoVereda);
				startActivity(intent);
				}
				
			}
		});
		
		btnAtras.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
					
	}	
		


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1 && resultCode == RESULT_OK) {
			
			Bundle extras = data.getExtras();
			SearchItem item = extras.getParcelable("item");
			
//	    	String item = parent.getItemAtPosition(position).toString();
	    	unaCalle = manager.getCalleByDescripcion(item.getText());
	    	idCalle = String.valueOf(unaCalle.getId());
			
			tvCalle.setText(item.getText());
			
		} else if(requestCode == 2 && resultCode == RESULT_OK) {
			Bundle extras = data.getExtras();
			SearchItem item = extras.getParcelable("item");
			unaEsquina = manager.getCalleByDescripcion(item.getText());
	    	idEsquina = String.valueOf(unaEsquina.getId());
			
			tvEsquina.setText(item.getText());
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
	

}
