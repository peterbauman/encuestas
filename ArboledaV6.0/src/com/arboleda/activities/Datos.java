package com.arboleda.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arboleda.R;
import com.arboleda.clases.Censista;
import com.arboleda.db.DatabaseManager;

public class Datos extends Activity {
	private EditText et;
	private Button btnSalir;
	private Button btnExportar;
	private EditText txtPassAdm;
	private String idEquipo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Set portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
		setContentView(R.layout.datos);

		btnSalir = (Button) findViewById(R.id.btnSalir);
		btnExportar = (Button) findViewById(R.id.btnExportar);
		txtPassAdm = (EditText) findViewById(R.id.txtAdm);

		et = (EditText) findViewById(R.id.txtIdEquipo);
		idEquipo = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
		et.setText(idEquipo);
		
		btnSalir.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
		
		btnExportar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (txtPassAdm.getText().toString().equals("123123")) {
					exportar();					
					final ProgressDialog dialog = ProgressDialog.show(Datos.this, "",
							"Exportando. Por favor espere...", true);
					new Thread(new Runnable() {
						public void run() {
							try {
								Thread.sleep(2500);
								dialog.dismiss();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}).start();
					
					txtPassAdm.setText("");
				} else {
					Toast toast = Toast.makeText(getApplicationContext(),
							"CONTRASEŅA INCORRECTA... Intente nuevavente.",
							Toast.LENGTH_LONG);
					toast.show();
				}				
			}
		});
		
	}

	public void exportar() {
		  
			File dbFolder = new File(Environment.getExternalStorageDirectory(), "CensoArbolado");
			 if (!dbFolder.exists()) 
		     {
		    	 dbFolder.mkdirs();
		     }

		  String filename = "arboles.db";
		        InputStream in = null;
		        OutputStream out = null;
		        try {
		          File inFile = new File(Environment.getDataDirectory().getPath()+"/data/com.arboleda/databases/", filename);
		          in = new FileInputStream(inFile);
		          File outFile = new File(dbFolder, "arboles_" + idEquipo + ".db");
		          out = new FileOutputStream(outFile);
		          copyFile(in, out);
		          in.close();
		          in = null;
		          out.flush();
		          out.close();
		          out = null;
		        } catch(IOException e) {
		            Log.e("tag", "Failed to copy asset file: " + filename, e);
		        }       
		    
		}

		private void copyFile(InputStream in, OutputStream out) throws IOException {
		    byte[] buffer = new byte[1024];
		    int read;
		    while((read = in.read(buffer)) != -1){
		      out.write(buffer, 0, read);
		    }
		}
	
}
