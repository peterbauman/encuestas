package com.arboleda;

import java.util.Timer;
import java.util.TimerTask;

import com.arboleda.activities.Inicio2;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

public class SplashScreenService extends Service {
	
    // Set the duration of the splash screen
    private static final long SPLASH_SCREEN_DELAY = 3000;
    
    public static final String SPLASH_SCREEN_FINISH_ACTION = "com.arboleda.splashscreen.finish.action";
 
    TimerTask task;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (task == null) {
			task = new TimerTask() {
				@Override
				public void run() {
	                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(SplashScreenService.this);
	                lbm.sendBroadcast(new Intent(SPLASH_SCREEN_FINISH_ACTION));

	                stopSelf();
	                
	                task = null;
				}
			};
			// Simulate a long loading process on application startup.
			Timer timer = new Timer();
			timer.schedule(task, SPLASH_SCREEN_DELAY);
		}

		return Service.START_NOT_STICKY;
	}
}
